var botaoAdicionar = document.querySelector("#adicionar-paciente");
botaoAdicionar.addEventListener("click", function(event) {
	event.preventDefault();
	var form = document.querySelector("#form-adiciona");

	var paciente = obtemPacienteDoForm(form);

	var erros = validaPaciente(paciente);

	if(erros.length > 0) {
		exibeMensagemDeErro(erros);
		return;
	}

	colocaPacienteNaTabela(paciente)
	
	form.reset();
	var ul = document.querySelector("#mensagem-erro");
	ul.innerHTML = "";
});


function colocaPacienteNaTabela(paciente) {
	var pacienteTr = obtemPacienteTr(paciente);

	var tabela = document.querySelector("#tabela-pacientes");
	tabela.appendChild(pacienteTr);
}


function obtemPacienteTr(paciente) {
	var pacienteTr = document.createElement("tr");

	pacienteTr.appendChild(montaTd(paciente.nome, "info-nome"));
	pacienteTr.appendChild(montaTd(paciente.peso, "info-peso"));
	pacienteTr.appendChild(montaTd(paciente.altura, "info-altura"));
	pacienteTr.appendChild(montaTd(paciente.gordura, "info-gordura"));
	pacienteTr.appendChild(montaTd(paciente.imc, "info-gordura"));

	return pacienteTr;
}


function obtemPacienteDoForm(form) {

	var paciente = {
		nome: form.nome.value,
		altura: form.altura.value,
		peso: form.peso.value,
		gordura: form.gordura.value,
		imc: calculaImc(form.peso.value, form.altura.value)
	}
	return paciente;
}

function montaTd(dado, classe) {
	var td = document.createElement("td");
	td.classList.add(classe);
	td.textContent = dado;
	return td;
}

function validaPaciente(paciente) {
	var erros = [];
	if(!validaPeso(paciente.peso)) erros.push("Peso é inválido");
	if(!validaAltura(paciente.altura)) erros.push("Altura é inválida");
	if(paciente.nome.length == 0) erros.push("o campo nome não pode ser vazio");
	if(paciente.peso.length == 0) erros.push("o campo peso não pode ser vazio");
	if(paciente.altura.length == 0) erros.push("o campo altura não pode ser vazio");
	if(paciente.gordura.length == 0) erros.push("o campo gordura não pode ser vazio");
	return erros;
}

function obtemErroLi(erro) {
	var liErro = document.createElement("li");
	liErro.textContent = erro;
	return liErro;
}

function exibeMensagemDeErro(erros) {
	var ul = document.querySelector("#lista-erro");
	ul.innerHTML = "";
	erros.forEach(function(erro) {
		ul.appendChild(obtemErroLi(erro));
	});

}



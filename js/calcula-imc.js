console.log("Works!")


var titulo = document.querySelector(".titulo")
titulo.textContent = "Aparecida Nutricionista"

var pacientes = document.querySelectorAll(".paciente")


for (var i = 0; i < pacientes.length; i++) {

	var paciente = pacientes[i]

	var tdPeso = paciente.querySelector(".info-peso")
	var peso = tdPeso.textContent

	var tdAltura = paciente.querySelector(".info-altura")
	var altura = tdAltura.textContent

	var tdImc = paciente.querySelector(".info-imc")

	var pesoEhvalido = validaPeso(peso);
	var alturaEhvalida = validaAltura(altura);
	if(!pesoEhvalido) {
		tdImc.textContent = "Peso inválido";
		pesoEhvalido = false;
		paciente.classList.add("paciente-invalido");
	}

	if(!alturaEhvalida) {
		tdImc.textContent = "Altura inválida";
		alturaEhvalida = false;
		paciente.classList.add("paciente-invalido")
	}

	if(pesoEhvalido && alturaEhvalida) {
		tdImc.textContent = calculaImc(peso, altura);
	}
}

function calculaImc(peso, altura) {
	var imc = peso /(altura * altura)
	return imc.toFixed(2)
}


function validaPeso(peso) {
	return peso > 0 && peso < 1000
}

function validaAltura(altura) {
	return altura > 0 && altura < 4.00
}



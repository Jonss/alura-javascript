console.log("Works!")


var titulo = document.querySelector(".titulo")
titulo.textContent = "Aparecida Nutricionista"

var pacientes = document.querySelectorAll(".paciente")


for (var i = 0; i < pacientes.length; i++) {

	var paciente = pacientes[i]

	var tdPeso = paciente.querySelector(".info-peso")
	var peso = tdPeso.textContent

	var tdAltura = paciente.querySelector(".info-altura")
	var altura = tdAltura.textContent


	var imc = peso /(altura * altura)

	var tdImc = paciente.querySelector(".info-imc")

	var pesoEhvalido = true;
	var alturaEhvalida = true;
	if(peso <= 0 || peso >= 1000) {
		tdImc.textContent = "Peso inválido";
		pesoEhvalido = false;
		paciente.classList.add("paciente-invalido");
	}

	if(altura <= 0 || altura >= 4.00) {
		tdImc.textContent = "Altura inválida";
		alturaEhvalida = false;
		paciente.classList.add("paciente-invalido")
	}

	if(pesoEhvalido && alturaEhvalida) {
		tdImc.textContent = imc.toFixed(2);
	}
}

var botaoAdicionar = document.querySelector("#adicionar-paciente");
botaoAdicionar.addEventListener("click", function(event) {
	event.preventDefault();
	var form = document.querySelector("#form-adiciona");

	var peso = form.peso.value;
	var altura = form.altura.value;
	var nome = form.nome.value;
	var gordura = form.gordura.value;

	var pacienteTr = document.createElement("tr");
	var nomeTd = document.createElement("td");
	var alturaTd = document.createElement("td")
	var gorduraTd = document.createElement("td")
	var pesoTd = document.createElement("td")
	var imcTd = document.createElement("td")

	nomeTd.textContent = nome
	pesoTd.textContent = peso
	gorduraTd.textContent = gordura
	alturaTd.textContent = altura
	alturaTd.classList.add("info-altura")
	pesoTd.classList.add("info-peso")
	imcTd.classList.add("info-imc")

	pacienteTr.appendChild(nomeTd)
	pacienteTr.appendChild(pesoTd)
	pacienteTr.appendChild(alturaTd)
	pacienteTr.appendChild(gorduraTd)
	pacienteTr.appendChild(imcTd)

	var tabela = document.querySelector("#tabela-pacientes")
	tabela.appendChild(pacienteTr)

})

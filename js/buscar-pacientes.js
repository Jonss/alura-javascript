var botaoBuscar = document.querySelector("#buscar-pacientes");

botaoBuscar.addEventListener("click", function() {
	
	console.log("Funciona");
	var xhr = new XMLHttpRequest();

	xhr.open("GET", "http://api-pacientes.herokuapp.com/pacientes";

	xhr.addEventListener("load", function() {
		var ajaxErro = document.querySelector("#erro-ajax");
		if(xhr.status == 200) {
			ajaxErro.classList.add("invisivel");
			var resposta = xhr.responseText;

			var pacientes = JSON.parse(resposta);

			pacientes.forEach(function(paciente){
				colocaPacienteNaTabela(paciente);
			});
		} else {
			console.log(xhr.status)
			console.log(xhr.responseText)
			ajaxErro.classList.remove("invisivel")
		}
		
	});

	xhr.send();
});